<table class="table table-responsive" id="prods-table">
    <thead>
        <tr>
            <th>Name</th>
        <th>Price</th>
        <th>Qty</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($prods as $prod)
        <tr>
            <td>{!! $prod->name !!}</td>
            <td>{!! $prod->price !!}</td>
            <td>{!! $prod->qty !!}</td>
            <td>
                {!! Form::open(['route' => ['prods.destroy', $prod->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('prods.show', [$prod->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('prods.edit', [$prod->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>