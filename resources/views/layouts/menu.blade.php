

<li class="{{ Request::is('prods*') ? 'active' : '' }}">
    <a href="{!! route('prods.index') !!}"><i class="fa fa-edit"></i><span>Prods</span></a>
</li>

<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{!! route('categories.index') !!}"><i class="fa fa-edit"></i><span>Categories</span></a>
</li>

