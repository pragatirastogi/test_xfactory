<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required'=>'required']) !!}
</div>

<!-- Slug Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug_url', 'Slug Url:') !!}
    {!! Form::text('slug_url', null, ['class' => 'form-control', 'required'=>'required']) !!}
</div>

<!-- Photo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::file('photo') !!}
</div>
<div class="clearfix"></div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ['1' => 'Enabled', '0' => 'Disabled'], null, ['class' => 'form-control', 'required'=>'required']) !!}
</div>

<!-- Parent Cat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('parent_cat', 'Parent Cat:') !!}
    {!! Form::select('parent_cat',$cats, null, ['class' => 'form-control', 'required'=>'required']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('categories.index') !!}" class="btn btn-default">Cancel</a>
</div>
