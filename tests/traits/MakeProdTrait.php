<?php

use Faker\Factory as Faker;
use App\Models\Prod;
use App\Repositories\ProdRepository;

trait MakeProdTrait
{
    /**
     * Create fake instance of Prod and save it in database
     *
     * @param array $prodFields
     * @return Prod
     */
    public function makeProd($prodFields = [])
    {
        /** @var ProdRepository $prodRepo */
        $prodRepo = App::make(ProdRepository::class);
        $theme = $this->fakeProdData($prodFields);
        return $prodRepo->create($theme);
    }

    /**
     * Get fake instance of Prod
     *
     * @param array $prodFields
     * @return Prod
     */
    public function fakeProd($prodFields = [])
    {
        return new Prod($this->fakeProdData($prodFields));
    }

    /**
     * Get fake data of Prod
     *
     * @param array $postFields
     * @return array
     */
    public function fakeProdData($prodFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'price' => $fake->randomDigitNotNull,
            'qty' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $prodFields);
    }
}
