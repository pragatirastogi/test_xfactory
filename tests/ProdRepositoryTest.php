<?php

use App\Models\Prod;
use App\Repositories\ProdRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProdRepositoryTest extends TestCase
{
    use MakeProdTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProdRepository
     */
    protected $prodRepo;

    public function setUp()
    {
        parent::setUp();
        $this->prodRepo = App::make(ProdRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateProd()
    {
        $prod = $this->fakeProdData();
        $createdProd = $this->prodRepo->create($prod);
        $createdProd = $createdProd->toArray();
        $this->assertArrayHasKey('id', $createdProd);
        $this->assertNotNull($createdProd['id'], 'Created Prod must have id specified');
        $this->assertNotNull(Prod::find($createdProd['id']), 'Prod with given id must be in DB');
        $this->assertModelData($prod, $createdProd);
    }

    /**
     * @test read
     */
    public function testReadProd()
    {
        $prod = $this->makeProd();
        $dbProd = $this->prodRepo->find($prod->id);
        $dbProd = $dbProd->toArray();
        $this->assertModelData($prod->toArray(), $dbProd);
    }

    /**
     * @test update
     */
    public function testUpdateProd()
    {
        $prod = $this->makeProd();
        $fakeProd = $this->fakeProdData();
        $updatedProd = $this->prodRepo->update($fakeProd, $prod->id);
        $this->assertModelData($fakeProd, $updatedProd->toArray());
        $dbProd = $this->prodRepo->find($prod->id);
        $this->assertModelData($fakeProd, $dbProd->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteProd()
    {
        $prod = $this->makeProd();
        $resp = $this->prodRepo->delete($prod->id);
        $this->assertTrue($resp);
        $this->assertNull(Prod::find($prod->id), 'Prod should not exist in DB');
    }
}
