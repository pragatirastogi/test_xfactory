<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProdApiTest extends TestCase
{
    use MakeProdTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateProd()
    {
        $prod = $this->fakeProdData();
        $this->json('POST', '/api/v1/prods', $prod);

        $this->assertApiResponse($prod);
    }

    /**
     * @test
     */
    public function testReadProd()
    {
        $prod = $this->makeProd();
        $this->json('GET', '/api/v1/prods/'.$prod->id);

        $this->assertApiResponse($prod->toArray());
    }

    /**
     * @test
     */
    public function testUpdateProd()
    {
        $prod = $this->makeProd();
        $editedProd = $this->fakeProdData();

        $this->json('PUT', '/api/v1/prods/'.$prod->id, $editedProd);

        $this->assertApiResponse($editedProd);
    }

    /**
     * @test
     */
    public function testDeleteProd()
    {
        $prod = $this->makeProd();
        $this->json('DELETE', '/api/v1/prods/'.$prod->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/prods/'.$prod->id);

        $this->assertResponseStatus(404);
    }
}
