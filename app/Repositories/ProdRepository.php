<?php

namespace App\Repositories;

use App\Models\Prod;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProdRepository
 * @package App\Repositories
 * @version November 23, 2017, 12:49 am UTC
 *
 * @method Prod findWithoutFail($id, $columns = ['*'])
 * @method Prod find($id, $columns = ['*'])
 * @method Prod first($columns = ['*'])
*/
class ProdRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'price',
        'qty'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Prod::class;
    }
}
