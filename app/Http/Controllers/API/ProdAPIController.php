<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProdAPIRequest;
use App\Http\Requests\API\UpdateProdAPIRequest;
use App\Models\Prod;
use App\Repositories\ProdRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ProdController
 * @package App\Http\Controllers\API
 */

class ProdAPIController extends AppBaseController
{
    /** @var  ProdRepository */
    private $prodRepository;

    public function __construct(ProdRepository $prodRepo)
    {
        $this->prodRepository = $prodRepo;
    }

    /**
     * Display a listing of the Prod.
     * GET|HEAD /prods
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->prodRepository->pushCriteria(new RequestCriteria($request));
        $this->prodRepository->pushCriteria(new LimitOffsetCriteria($request));
        $prods = $this->prodRepository->all();

        return $this->sendResponse($prods->toArray(), 'Prods retrieved successfully');
    }

    /**
     * Store a newly created Prod in storage.
     * POST /prods
     *
     * @param CreateProdAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProdAPIRequest $request)
    {
        $input = $request->all();

        $prods = $this->prodRepository->create($input);

        return $this->sendResponse($prods->toArray(), 'Prod saved successfully');
    }

    /**
     * Display the specified Prod.
     * GET|HEAD /prods/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Prod $prod */
        $prod = $this->prodRepository->findWithoutFail($id);

        if (empty($prod)) {
            return $this->sendError('Prod not found');
        }

        return $this->sendResponse($prod->toArray(), 'Prod retrieved successfully');
    }

    /**
     * Update the specified Prod in storage.
     * PUT/PATCH /prods/{id}
     *
     * @param  int $id
     * @param UpdateProdAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProdAPIRequest $request)
    {
        $input = $request->all();

        /** @var Prod $prod */
        $prod = $this->prodRepository->findWithoutFail($id);

        if (empty($prod)) {
            return $this->sendError('Prod not found');
        }

        $prod = $this->prodRepository->update($input, $id);

        return $this->sendResponse($prod->toArray(), 'Prod updated successfully');
    }

    /**
     * Remove the specified Prod from storage.
     * DELETE /prods/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Prod $prod */
        $prod = $this->prodRepository->findWithoutFail($id);

        if (empty($prod)) {
            return $this->sendError('Prod not found');
        }

        $prod->delete();

        return $this->sendResponse($id, 'Prod deleted successfully');
    }
}
