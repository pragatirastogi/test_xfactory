<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProdRequest;
use App\Http\Requests\UpdateProdRequest;
use App\Repositories\ProdRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Category;
use App\Prodcategory;

class ProdController extends AppBaseController
{
    /** @var  ProdRepository */
    private $prodRepository;

    public function __construct(ProdRepository $prodRepo)
    {
        $this->prodRepository = $prodRepo;
    }

    /**
     * Display a listing of the Prod.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->prodRepository->pushCriteria(new RequestCriteria($request));
        $prods = $this->prodRepository->all();

        return view('prods.index')
            ->with('prods', $prods);
    }

    /**
     * Show the form for creating a new Prod.
     *
     * @return Response
     */
    public function create()
    {
		$cats = Category::lists(['id', 'name']);
        return view('prods.create',['cats' => $cats]);
    }

    /**
     * Store a newly created Prod in storage.
     *
     * @param CreateProdRequest $request
     *
     * @return Response
     */
    public function store(CreateProdRequest $request)
    {
        $input = $request->all();
		
        $prod = $this->prodRepository->create($input);
		$prod_id = $prod['id'];
        $prod_cats = $input['categories'];
		$data = array();
		foreach($prod_cats as $pc){
			$data['prod_id'] = $prod_id;
			$data['cat_id'] = $pc;
		}
		Prodcategory::insert($data);
		

        Flash::success('Prod saved successfully.');

        return redirect(route('prods.index'));
    }

    /**
     * Display the specified Prod.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $prod = $this->prodRepository->findWithoutFail($id);

        if (empty($prod)) {
            Flash::error('Prod not found');

            return redirect(route('prods.index'));
        }

        return view('prods.show')->with('prod', $prod);
    }

    /**
     * Show the form for editing the specified Prod.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
		$cats = Category::lists(['id', 'name']);
        $prod = $this->prodRepository->findWithoutFail($id);

        if (empty($prod)) {
            Flash::error('Prod not found');

            return redirect(route('prods.index'));
        }

        return view('prods.edit',['cats' => $cats])->with('prod', $prod);
    }

    /**
     * Update the specified Prod in storage.
     *
     * @param  int              $id
     * @param UpdateProdRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProdRequest $request)
    {
        $prod = $this->prodRepository->findWithoutFail($id);
		
        if (empty($prod)) {
            Flash::error('Prod not found');

            return redirect(route('prods.index'));
        }

        $prod = $this->prodRepository->update($request->all(), $id);
        $input = $request->all();
		$prod_id = $prod['id'];
        $prod_cats = $input['categories'];
		$data = array();
		foreach($prod_cats as $pc){
			$data['prod_id'] = $prod_id;
			$data['cat_id'] = $pc;
		}
		Prodcategory::where('prod_id',$prod_id)->delete();
		Prodcategory::insert($data);
        Flash::success('Prod updated successfully.');

        return redirect(route('prods.index'));
    }

    /**
     * Remove the specified Prod from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $prod = $this->prodRepository->findWithoutFail($id);

        if (empty($prod)) {
            Flash::error('Prod not found');

            return redirect(route('prods.index'));
        }

        $this->prodRepository->delete($id);

        Flash::success('Prod deleted successfully.');

        return redirect(route('prods.index'));
    }
}
