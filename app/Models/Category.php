<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App\Models
 * @version November 23, 2017, 1:00 am UTC
 *
 * @property string name
 * @property string slug_url
 * @property string photo
 * @property integer status
 * @property integer parent_cat
 */
class Category extends Model
{
    use SoftDeletes;

    public $table = 'categories';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'slug_url',
        'photo',
        'status',
        'parent_cat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'slug_url' => 'string',
        'photo' => 'string',
        'status' => 'integer',
        'parent_cat' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'slug_url' => 'required',
        'status' => 'required',
        'parent_cat' => 'required'
    ];

    
}
