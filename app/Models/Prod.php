<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Prod
 * @package App\Models
 * @version November 23, 2017, 12:49 am UTC
 *
 * @property string name
 * @property integer price
 * @property integer qty
 */
class Prod extends Model
{
    use SoftDeletes;

    public $table = 'prods';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'price',
        'qty'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'price' => 'integer',
        'qty' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'price' => 'numeric',
        'qty' => 'integer'
    ];

    
}
